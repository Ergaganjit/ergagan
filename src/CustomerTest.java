import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CustomerTest {

	@Test
	void testCustomerHasAccount() {
	
		Customer customer=new Customer("Gagan",1000);
		Account customerHasAccount=customer.getAccount();
		assertNotNull(customerHasAccount);
	}@Test
	public void testCustomerHasChoice() {
		Customer customer = new Customer("Gagan", 200);
		Account account = customer.getAccount();
		int balanceAfterOpening = account.balance();
		assertEquals(200, balanceAfterOpening);
		}
	@Test
	public void testCustomerHasChoiceToInitializeWithZero() {
	Customer customer = new Customer("Gagan", 0);
	Account account = customer.getAccount();
	int balanceAfterOpening = account.balance();
	assertEquals(0, balanceAfterOpening);
	}

}
