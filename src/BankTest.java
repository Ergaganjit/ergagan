import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class BankTest {

	@Test
	void testInitialBankCustomerAsZero() {
		Bank bk=new Bank();
		int initialCustomers=bk.getNumberOfCustomers();
		assertEquals(0, initialCustomers);
	}
	@Test
	void testAddCustomer( ) {
		Bank bk=new Bank();
		int initialCustomer=bk.getNumberOfCustomers();
		bk.addCustomer("Gagan",1000);
		int afterAdditionCustomers=bk.getNumberOfCustomers();
		assertEquals(initialCustomer+1,afterAdditionCustomers);
	}
	@Test
	void testRemoveCustomer( ) {
		Bank bk=new Bank();
		int initialCustomer=bk.getNumberOfCustomers();
		bk.addCustomer("Gagan",1000);
		bk.addCustomer("Nilanshi",1000);
		bk.addCustomer("Harpreet",1000);
		bk.addCustomer("Karam",1000);
		int afterAdditionCustomers=bk.getNumberOfCustomers();
		bk.removeCustomer("Gagan");
		int afterRemovalCustomers=bk.getNumberOfCustomers();
		assertEquals(afterAdditionCustomers-1,afterRemovalCustomers);
	}
	@Test
	void testMoneyTransferBetweenAccounts() {
		Bank bk=new Bank();
		bk.addCustomer("Gagan",1200);
		bk.addCustomer("Harpreet",800);
		ArrayList<Customer> arrayList=bk.getCustomers();
		Customer customer1=arrayList.get(0);
		Customer customer2=arrayList.get(1);
		bk.transferMoney(customer1,customer2, 400);
		Account customer1Account=customer1.getAccount();
		Account customer2Account=customer2.getAccount();
		int customer1AccountBalance=customer1Account.balance();
		int customer2AccountBalance=customer2Account.balance();
		assertEquals(1200, customer2AccountBalance);
		assertEquals(800,customer1AccountBalance);
	}
	

}
